import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  notFoundPictureUrl = 'assets/a1a7fb209bbadb19d1ae6a9375c8e945.png'

  constructor() { }

  ngOnInit() {
  }

}

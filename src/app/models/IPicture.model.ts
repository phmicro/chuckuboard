export interface Picture{
    $key?: string; //Will be like ID for Firebase Reasons
    name?: string;
    url?: string;
}
import { Component, OnInit } from '@angular/core';
import { PictureService } from '../services/picture.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent implements OnInit {

  picture: any;

  constructor(private pictureService: PictureService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.picture = this.pictureService.getPicture(
      this.route.snapshot.params['key']
    )
  }

}

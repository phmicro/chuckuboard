import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Picture } from '../models/IPicture.model';

import { PictureService } from '../services/picture.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit, OnChanges {
  
  @Input() filterBy?: string ='all';

  visiblePictures: Observable<Picture[]>;

  constructor(private pictureService: PictureService) { 
    this.visiblePictures = this.pictureService.getPictures();
  }


  ngOnChanges(){
    this.visiblePictures = this.pictureService.getPictures();
  }

  ngOnInit() {
    this.visiblePictures = this.pictureService.getPictures();
  }

}

import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { Picture } from '../models/IPicture.model';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Upload } from '../models/IUpload.model';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private basePath = '/uploads';
  private uploads: AngularFireList<Picture[]>;

  constructor(private ngFire: AngularFireModule, private db: AngularFireDatabase) { }

  uploadFile(upload: Upload){
    const fileName = new Date().getTime() + upload.file.name;

    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${fileName}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      //stateChange Observer:
      (snapshot) => {
        upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100; //ProgressBar
        console.log(upload.progress);
      },
      //error Observer
      (error) => {
        console.log("ERROR while uploading", error);
      },
      //Success Observer
      (): any => {
        //upload.url = uploadTask.snapshot.downloadURL;
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
          upload.url = downloadURL.toString();
          upload.name = fileName;
          this.saveFileData(upload);
        });
        
        
      }
    );
  }

  private saveFileData(upload: Upload){
    this.db.list(`${this.basePath}/`).push(upload);
    console.log("Files are uploaded!: " + upload.url);
  }
}

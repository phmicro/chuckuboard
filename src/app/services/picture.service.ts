import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import 'firebase/storage';
import * as firebase from 'firebase';

import { Picture } from '../models/IPicture.model';


@Injectable({
  providedIn: 'root'
})
export class PictureService {
  private uid: string;

  constructor(private fireAuth: AngularFireAuth, private db: AngularFireDatabase) { 
    this.fireAuth.authState.subscribe(
      auth => {
        if (auth !== undefined && auth !== null){
          this.uid = auth.uid;
        }
      }
    );
  }


  visiblePictures = [];

  getPictures(): Observable<Picture[]>{
    console.log("In getPictures()");
    //return this.visiblePictures = PICTURES.slice(0); //deprecated
    this.db.list('uploads').valueChanges().subscribe( (data) => {
      console.log(data);
    })
    return this.db.list('uploads').valueChanges() as Observable<Picture[]>; //FirebaseMagic!
  }

  getPicture(key: string){ //deprecated
    
  }
}

const PICTURES = [
    { "id": 1, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter01.jpeg"},
    { "id": 2, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter02.jpg"},
    { "id": 3, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter03.jpg"},
    { "id": 4, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter04.jpg"},
    { "id": 5, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter05.JPG"},
    { "id": 6, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter06.jpg"},
    { "id": 7, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter07.jpg"},
    { "id": 8, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter08.jpg"},
    { "id": 9, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter09.jpg"},
    { "id": 10, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter10.jpg"},
    { "id": 11, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter11.jpg"},
    { "id": 12, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter12.jpg"},
    { "id": 13, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter13.jpg"},
    { "id": 14, "Place": "land", "Description": "Otter", "url": "assets/testpics/Otter14.jpg"},
    { "id": 15, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter15.jpg"},
    { "id": 16, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter16.jpg"},
    { "id": 17, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter17.jpg"},
    { "id": 18, "Place": "water", "Description": "Otter", "url": "assets/testpics/Otter18.jpg"}
]

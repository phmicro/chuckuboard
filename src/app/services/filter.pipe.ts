//Will be deleted!

import {Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'pictureFilter'})
export class PictureFilterPipe implements PipeTransform{
    transform(items: any[], criteria: string): any {
        if(criteria == 'all') {return items } else
        return items.filter(item => {
            return item.Place === criteria;
        });
    }
}
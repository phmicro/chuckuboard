import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';

import * as firebase  from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  user: Observable<firebase.User>;


  constructor(private afAuth: AuthService, private router: Router) { 
    //this.user = afAuth.authState.pipe(first());
   }

   canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    
    console.log(this.afAuth.authenticated());  
    
    if (this.afAuth.authenticated()) { return true; }

      console.log('access denied!')
      this.router.navigate(['/login']);
      return false
  }

  //###########
  //https://angularfirebase.com/lessons/router-guards-to-redirect-unauthorized-firebase-users/
    //  return this.user.map( //map might be wrong
    //    (auth) => {
    //      if (!auth){
    //        this.router.navigateByUrl('/login'); //If not logged in -> login
    //        return false;
    //      }
    //      return true;
    //    }
    //  ).take(1);



    // return this.user.pipe(
    //   tap(user => {
    //     if (user) {
    //       return true;
    //     } else {
    //       this.router.navigateByUrl('/login'); //If not logged in -> login
    //       return false;
    //     }
    //   })
    // )
    // .subscribe()


   
}

import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { User } from '../models/IUser.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: Observable<firebase.User>;
  private loggedin: firebase.User;

  constructor(private fireAuth: AngularFireAuth) {
    this.user = fireAuth.authState;

    this.user.subscribe(user => {
      console.log(user, "  <- user");
      if(user !== null) {
          this.loggedin = user;
      } else {
          this.loggedin = null;
      }
  });
  }

  login(user: User){
    return this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password); //Maybe needs to be changed for fixed auth!
  }

  logout(){
    return this.fireAuth.auth.signOut();
  }

  authUser(){
    return this.user;
  }

  authenticated(): boolean {
    console.log(this.loggedin);
    console.log((this.loggedin !== null) + " null")
    console.log((this.loggedin !== undefined)  + " undefined")
    return this.loggedin !== null && this.loggedin !== undefined;
  }

  get currentUserObservable(): any {
    return this.fireAuth.auth
  }
}

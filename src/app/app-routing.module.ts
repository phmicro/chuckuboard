import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GalleryComponent } from './gallery/gallery.component';
import { PictureComponent } from './picture/picture.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { UploadComponent } from './upload/upload.component';

import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/gallery', pathMatch: 'full'},
  { path: 'gallery' , component: GalleryComponent }, //, canActivate: [AuthGuardService] 
  { path: 'upload' , component: UploadComponent, canActivate: [AuthGuardService] },//, canActivate: [AuthGuardService] 
  { path: 'login' , component: LoginComponent },
  { path: 'picture/:id' , component: PictureComponent },//, canActivate: [AuthGuardService] 
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  email: string;
  password: string;
  error: string;

  constructor(private authService: AuthService, private router: Router) { 

  }

  signIn(){
    this.authService.login({ email: this.email, password: this.password})
      .then(resolve => this.router.navigate(['gallery']))
      .catch(error => this.error = error.message);
  }

}
